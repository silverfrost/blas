### BLAS using the 32-bit library

This example links to a 32-bit BLAS static library and calls a BLAS routine from Fortran. The project includes a PLATO
project file from which you can load the project into PLATO. There is also a batch file you can use
to manually rebuild the example.

The BLAS library was built using [Silverfrost Fortran FTN95](https://www.silverfrost.com/11/ftn95/ftn95_fortran_95_for_windows.aspx) and the source code originates here: 
[https://github.com/Reference-LAPACK/lapack-release/tree/lapack-3.8.0/BLAS/SRC](https://github.com/Reference-LAPACK/lapack-release/tree/lapack-3.8.0/BLAS/SRC)
