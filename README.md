### Using BLAS with Silverfrost Fortran

Presented here are examples of how to use the BLAS library in your FTN95 code. It is very simple - you add the static library to your
project and then use the subroutines. If you are compiling without being concerned for 32- or 64-bit code with Silverfrost Fortran then use the 32-bit example. 

The BLAS libraries here were built using [Silverfrost Fortran FTN95](https://www.silverfrost.com/11/ftn95/ftn95_fortran_95_for_windows.aspx) and the source code originates here: 
[https://github.com/Reference-LAPACK/lapack-release/tree/lapack-3.8.0/BLAS/SRC](https://github.com/Reference-LAPACK/lapack-release/tree/lapack-3.8.0/BLAS/SRC)